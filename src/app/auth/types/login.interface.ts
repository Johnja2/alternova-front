import { IUser } from "src/app/common/types/user.interface"


export interface ILogin {
    email: string,
    password: string
}

export interface ILoginResponse {
    token: string,
    uuid: string,
    user: IUser
} 