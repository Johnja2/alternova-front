import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { catchError, map, Observable, of, tap } from 'rxjs';
import { ResponseDto } from 'src/app/common/types/reponse-dto.interface';
import { environment } from 'src/environments/environment';
import { ILogin, ILoginResponse } from '../types/login.interface';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public baseUrl = environment.baseUrl;

  constructor(private http: HttpClient, private router: Router) {
  }

  public login(loginData: ILogin) {
    return this.http.post<ResponseDto<ILoginResponse>>(`${this.baseUrl}/login`, loginData).pipe(
      tap((response: any) => {
        localStorage.setItem('token', response.token);
        localStorage.setItem('uuid', response.uuid);
      })
    );
  }

  /**
   * Method for log out
   */
  public logOut() {
    localStorage.removeItem('token');
    localStorage.removeItem('uuid');
    this.router.navigateByUrl('/login');
  }

  /**
   * Method for validate token
   */
  validateTokenServer() {
    const token = localStorage.getItem('token') || '';
    return this.http.get(`${this.baseUrl}/login/renew`, {
      headers: {
        'Autorization': token
      }
    }).pipe(
      tap((response: any) => {
        localStorage.setItem('token', response.token)
      }),
      map((response: any) => true),
      catchError((error) => of(false))
    );
  }

}
