import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { ToastrService } from 'ngx-toastr';
import { tap } from 'rxjs';
import { UserService } from 'src/app/common/services/user.service';
import { IUser } from 'src/app/common/types/user.interface';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public emailPattern: any = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  public registerForm!: FormGroup;

  constructor(private toastr: ToastrService, private userService: UserService, private router: Router, private fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.registerFormBuilder();
  }

  private registerFormBuilder() {
    this.registerForm = this.fb.group({
      fullName: new FormControl('', Validators.required),
      documentType: new FormControl('', Validators.required),
      documentId: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.pattern(this.emailPattern)]),
      password: new FormControl('', Validators.required),
    });
  }

  public submit() {
    try {
      if (this.registerForm.valid) {
        const userData: IUser = {
          fullName: this.registerForm.get('fullName')?.value,
          documentType: this.registerForm.get('documentType')?.value,
          documentId: this.registerForm.get('documentId')?.value,
          email: this.registerForm.get('email')?.value,
          password: this.registerForm.get('password')?.value,
        }
        this.userService.createUser(userData).pipe(
          tap((response: any) => {
            if (response.status === 'success') {
              this.router.navigateByUrl('/login');
              this.toastr.success('User was create', 'Success');
            } else {
              this.toastr.error('Check the information send', 'Error');
            }
          })
        ).subscribe({
          next: (res) => {
          },
          error: (err) => {
          },
        });
      } else {
        this.toastr.error('Check the fields', 'Error');
      }
    } catch (err: any) {
      console.error(err);
      this.toastr.error(err, 'Error');
    }
  }
}


