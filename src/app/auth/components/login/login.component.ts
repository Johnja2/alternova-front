import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { ToastrService } from 'ngx-toastr';
import { tap } from 'rxjs';
import { AuthService } from "../../services/auth.service";
import { ILogin } from '../../types/login.interface';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public emailPattern: any = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  public loginForm!: FormGroup;

  constructor(private toastr: ToastrService, private authService: AuthService,
    private router: Router, private fb: FormBuilder) {
  }

  ngOnInit() {
    this.loginFormBuilder();
  }

  private loginFormBuilder() {
    this.loginForm = this.fb.group({
      email: new FormControl('', [Validators.required, Validators.pattern(this.emailPattern)]),
      password: new FormControl('', Validators.required),
    });
  }

  public submit() {
    try {
      if (this.loginForm.valid) {
        const loginData: ILogin = {
          email: this.loginForm.get('email')?.value,
          password: this.loginForm.get('password')?.value,
        }
        this.authService.login(loginData).pipe(
          tap((response: any) => {
            if (response.status === 'success') {
              this.authService.validateTokenServer();
              this.router.navigateByUrl('/');
              this.toastr.success('Enter to Alternova', 'Success');
            } else {
              this.toastr.error('Check the information send', 'Error');
            }
          }
          )
        ).subscribe({
          next: (res) => {
          },
          error: (err) => {
          },
        });
      } else {
        console.error('error');
        this.toastr.error('Check the fields', 'Error');
      }
    } catch (e: any) {
      console.error(e);
      this.toastr.error(e, 'Error');
    }
  }
}
