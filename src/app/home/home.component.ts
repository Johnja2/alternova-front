import { Component, OnInit } from '@angular/core';
import { tap } from 'rxjs';
import { AuthService } from '../auth/services/auth.service';
import { UserService } from '../common/services/user.service';
import { IUser } from '../common/types/user.interface';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public name = '';
  public email = '';

  constructor(private authService: AuthService, private userService: UserService) {
  }

  ngOnInit() {
    this.getUserInformation();
  }

  public logout() {
    this.authService.logOut();
  }

  public getUserInformation() {

    const uuid = localStorage.getItem('uuid');
    if (uuid !== null) {
      this.userService.getUserById(uuid).pipe(
        tap((data) => {
          this.name = data.response.fullName;
          this.email = data.response.email
        })
      ).subscribe({
        next: (res) => {
        },
        error: (err) => {
        },
      });
    }

  }

}
