export interface ResponseDto<T> {
    error: boolean;
    message: string;
    response: T;
}