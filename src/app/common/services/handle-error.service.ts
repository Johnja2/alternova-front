import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HandleErrorService {

  constructor() { }

  public handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      console.error('Ha ocurrido un error:', error.error);
    } else {
      console.error(`Backend  ha retornado un código ${error.status}. Body: `, error.error);
    }
    return throwError(() => new Error('Ocurrió un error; por favor intente más tarde.'));
  }
}
