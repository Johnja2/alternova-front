import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from 'src/environments/environment';
import { IUser } from 'src/app/common/types/user.interface';
import { catchError } from 'rxjs';
import { HandleErrorService } from './handle-error.service';
import { ResponseDto } from '../types/reponse-dto.interface';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient, private handleErrorService: HandleErrorService) { }

  get token(): string {
    return localStorage.getItem('token') || '';
  }

  get uuid(): string {
    return localStorage.getItem('uuid') || '';
  }

  get headers() {
    return {
      headers: {
        'Authorization': this.token
      }
    }
  }

  public createUser(userData: IUser) {
    return this.http.post<ResponseDto<IUser>>(`${this.baseUrl}/actors`, userData, this.headers).pipe(
      catchError(
        this.handleErrorService.handleError
      )
    );
  }

  public getUserById(id: string) {
    return this.http.get<ResponseDto<IUser>>(`${this.baseUrl}/actors/${id}`, this.headers).pipe(
      catchError(
        this.handleErrorService.handleError
      )
    );
  }
}
