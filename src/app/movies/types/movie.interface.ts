export interface IMovie {
    id?: string,
    name: string,
    movieGender: MovieGender;
    movieType: MovieType;
    viewNumber: number;
    score: number;
}

export interface IMovieUpdate {
    score?: number,
    viewNumber?: number
}

export enum MovieGender {
    ACTION,
    ANIMATION,
    BIOPIC,
    CARTOON,
    COMEDY,
    DRAMA,
    EDUCATIONAL,
    DOCUMENTARY,
    FANTASY,
    HORROR,
    ROMANTIC,
    SCARY,
    WAR,
    THRILLER
}

export enum MovieType {
    MOVIE,
    SERIE,
}