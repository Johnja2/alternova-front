import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { concatMap, tap } from 'rxjs';
import { MovieService } from '../../services/movie.service';
import { IMovie, MovieGender, MovieType } from '../../types/movie.interface';
import { CreateMovieComponent } from '../create-movie/create-movie.component';
import { FindMovieComponent } from '../find-movie/find-movie.component';
import { UpdateMovieComponent } from '../update-movie/update-movie.component';

@Component({
  selector: 'app-view-movies',
  templateUrl: './view-movies.component.html',
  styleUrls: ['./view-movies.component.scss']
})
export class ViewMoviesComponent implements OnInit {
  public headerTable: string[] = ['name', 'movieGender', 'movieType', 'viewNumber', 'score', 'actions'];
  public data = [
    {
      id: '1',
      name: 'La casa de Papel',
      movieGender: 'DRAMA',
      movieType: 'SERIE',
      viewNumber: 1020,
      score: 4.5
    },
    {
      id: '2',
      name: 'Star wars',
      movieGender: 'FICTION',
      movieType: 'MOVIE',
      viewNumber: 1020,
      score: 4.7
    },
  ]
  public actions: string[] = [];

  constructor(private movieService: MovieService, public dialog: MatDialog,
    private toastr: ToastrService) {
  }

  ngAfterViewInit() {
    this.getAllMovies();
  }

  ngOnInit() {
    this.getAllMovies();
  }


  public getAllMovies() {
    try {
      this.movieService.getAllMovies().pipe(
        tap((data) => {
          //  this.data = data.response;

        })
      ).subscribe({
        next: (res) => {
        },
        error: (err) => {
        },
      });
    } catch (e: any) {
      console.error(e);
      this.toastr.error(e, 'Error')
    }
  }


  public viewMovie(event: any) {
    if (event) {
      const dialogRef = this.dialog.open(FindMovieComponent, {
        width: '500px',
        data: event
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.dialog.closeAll();
        }
      });
    }
  }

  public updateMovie(event: any) {
    if (event) {
      const dialogRef = this.dialog.open(UpdateMovieComponent, {
        width: '500px',
        data: event
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.dialog.closeAll();
        }
      });
    }
  }

  public addIMovie(event: any) {
    if (event) {
      const dialogRef = this.dialog.open(CreateMovieComponent, {
        width: '500px',
      }
      );
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.dialog.closeAll();
        }
      });
    }
  }

  public async scoreMovie(event: any) {
    if (event) {
      const dialogRef = this.dialog.open(UpdateMovieComponent, {
        width: '500px',
        data: event
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.dialog.closeAll();
        }
      });
    }
  }
}
