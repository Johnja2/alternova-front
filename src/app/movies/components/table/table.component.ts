import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { IMovie } from 'src/app/movies/types/movie.interface';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit, AfterViewInit {
  @Input() headerTable: string[] = [];
  @Input() data: any;
  @Input() actions: string[] = [];
  @Output() viewMovie: EventEmitter<IMovie> = new EventEmitter<IMovie>();
  @Output() updateMovie: EventEmitter<IMovie> = new EventEmitter<IMovie>();
  @Output() scoreMovie: EventEmitter<IMovie> = new EventEmitter<IMovie>();
  @Output() addIMovie: EventEmitter<boolean> = new EventEmitter<boolean>();
  @ViewChild('TablePaginator', { static: true }) tablePaginator!: MatPaginator;
  public movieTable!: MatTableDataSource<IMovie>;

  constructor() {
  }

  ngOnInit(): void {
    this.movieTable = new MatTableDataSource(this.data);
    this.movieTable.paginator = this.tablePaginator;
  }

  ngAfterViewInit() {
    this.movieTable = new MatTableDataSource(this.data);
    this.movieTable.paginator = this.tablePaginator;
  }

  public applyFilterTable(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.movieTable.filter = filterValue.trim().toLowerCase();
    if (this.movieTable.paginator) {
      this.movieTable.paginator.firstPage();
    }
  }

  async getAction(element: any, action: string) {
    switch (action) {
      case 'view': {
        this.viewMovie.emit(element);
        break;
      }
      case 'update': {
        this.updateMovie.emit(element);
        break;
      }
      case 'score': {
        this.scoreMovie.emit(element);
        break;
      }
    }
  }

  public addMovie() {
    this.addIMovie.emit(true);
  }

}
