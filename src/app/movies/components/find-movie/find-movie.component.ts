import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { MovieService } from '../../services/movie.service';
import { IMovie } from '../../types/movie.interface';
import { UpdateMovieComponent } from '../update-movie/update-movie.component';

@Component({
  selector: 'app-find-movie',
  templateUrl: './find-movie.component.html',
  styleUrls: ['./find-movie.component.scss']
})
export class FindMovieComponent implements OnInit {
  public movieData!: IMovie;

  constructor(public dialogRef: MatDialogRef<UpdateMovieComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IMovie, private toastr: ToastrService,
    private movieService: MovieService) {
  }

  ngOnInit() {
  }


  public closeDialog(): void {
    this.dialogRef.close();
  }

  public async onSubmit() {
    this.closeDialog()
  }
}
