import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { tap } from 'rxjs';
import { MovieService } from '../../services/movie.service';
import { IMovie } from '../../types/movie.interface';


@Component({
  selector: 'app-create-movie',
  templateUrl: './create-movie.component.html',
  styleUrls: ['./create-movie.component.scss']
})
export class CreateMovieComponent implements OnInit {

  public form!: FormGroup;
  public movieGenders: any = [
    { label: 'ACTION', value: 'ACTION' },
    { label: 'ANIMATION', value: 'ANIMATION' },
    { label: 'BIOPIC', value: 'BIOPIC' },
    { label: 'CARTOON', value: 'CARTOON' },
    { label: 'COMEDY', value: 'COMEDY' },
    { label: 'DRAMA', value: 'DRAMA' },
    { label: 'EDUCATIONAL', value: 'EDUCATIONAL' },
    { label: 'DOCUMENTARY', value: 'DOCUMENTARY' },
    { label: 'FANTASY', value: 'FANTASY' },
    { label: 'HORROR', value: 'HORROR' },
    { label: 'ROMANTIC', value: 'ROMANTIC' },
    { label: 'SCARY', value: 'SCARY' },
    { label: 'WAR', value: 'WAR' },
    { label: 'THRILLER', value: 'THRILLER' },
  ]

  public movieTypes: any = [
    { label: 'MOVIE', value: 'MOVIE' },
    { label: 'SERIE', value: 'SERIE' },

  ]
  constructor(public dialogRef: MatDialogRef<CreateMovieComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IMovie, private toastr: ToastrService, private fb: FormBuilder,
    private movieService: MovieService) {
  }

  ngOnInit() {
    this.formBuilder();
  }

  private formBuilder() {
    this.form = this.fb.group({
      name: new FormControl('', Validators.required),
      movieGender: new FormControl('', Validators.required),
      movieType: new FormControl('', Validators.required),
      score: new FormControl(''),
      markAsView: new FormControl(''),
    });
  }

  public closeDialog(): void {
    this.dialogRef.close();
  }

  public async onSubmit() {
    try {
      if (this.form.valid) {
        const movieData: IMovie = {
          name: this.form.get('name')?.value,
          movieGender: this.form.get('movieGender')?.value,
          movieType: this.form.get('movieType')?.value,
          score: this.form.get('score')?.value,
          viewNumber: this.form.get('markAsView')?.value === true ? 1 : 0,
        };
        this.movieService.createMovie(movieData).pipe(
          tap((response) => {
            this.closeDialog();
          })
        ).subscribe({
          next: (res) => {
            this.closeDialog();
          },
          error: (err) => {
            this.closeDialog();
          },
        });

      } else {
        this.toastr.error('Error', 'Check the fields');
      }
    } catch (error) {
      console.error(error);
      this.toastr.error('Error', 'Check the fields');
    }
  }
}
