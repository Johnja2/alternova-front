import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { tap } from 'rxjs';
import { MovieService } from '../../services/movie.service';
import { IMovie, IMovieUpdate } from '../../types/movie.interface';

@Component({
  selector: 'app-update-movie',
  templateUrl: './update-movie.component.html',
  styleUrls: ['./update-movie.component.scss']
})
export class UpdateMovieComponent implements OnInit {

  public form!: FormGroup;
  public movieData!: IMovieUpdate;

  constructor(public dialogRef: MatDialogRef<UpdateMovieComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IMovie, private toastr: ToastrService, private fb: FormBuilder,
    private movieService: MovieService) {
  }

  ngOnInit() {
    this.formBuilder();
  }

  private formBuilder() {
    this.form = this.fb.group({
      score: new FormControl('', [Validators.min(0), Validators.max(5)]),
      markAsView: new FormControl(''),
    });
  }

  public closeDialog(): void {
    this.dialogRef.close();
  }

  public async onSubmit() {
    try {
      if (this.form.get('score')?.value || this.form.get('markAsView')?.value) {
        this.movieData.score = this.form.get('score')?.value;
        this.movieData.viewNumber = this.form.get('markAsView')?.value === true ? 1 : 0;
      };
      if (this.movieData) {
        this.movieService.markAsViewOrScoreMovie(this.movieData).pipe(
          tap((response) => { })
        ).subscribe({
          next: (res) => {
          },
          error: (err) => {
          },
        });
      }
      this.closeDialog()
    } catch (error) {
      console.error(error);
      this.toastr.error('Error', 'Check the fields');
    }
  }
}
