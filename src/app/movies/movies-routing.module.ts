import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateMovieComponent } from './components/create-movie/create-movie.component';
import { FindMovieComponent } from './components/find-movie/find-movie.component';
import { ViewMoviesComponent } from './components/view-movies/view-movies.component';
import { MoviesComponent } from './movies.component';

const routes: Routes = [
  {
    path: '',
    component: MoviesComponent,
    children: [
      {
        path: 'view-movies',
        component: ViewMoviesComponent,
      },
      {
        path: 'create-movie',
        component: CreateMovieComponent,
      },
      {
        path: 'find-movie',
        component: FindMovieComponent,
      },
      {
        path: '',
        redirectTo: 'view-movies',
        pathMatch: 'full'
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MoviesRoutingModule { }
