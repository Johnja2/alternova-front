import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MoviesRoutingModule } from './movies-routing.module';
import { ViewMoviesComponent } from './components/view-movies/view-movies.component';
import { FindMovieComponent } from './components/find-movie/find-movie.component';
import { MoviesComponent } from './movies.component';
import { CreateMovieComponent } from './components/create-movie/create-movie.component';
import { MaterialModule } from '../material/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { UpdateMovieComponent } from './components/update-movie/update-movie.component';
import { TableComponent } from './components/table/table.component';


@NgModule({
  declarations: [
    ViewMoviesComponent,
    FindMovieComponent,
    MoviesComponent,
    CreateMovieComponent,
    UpdateMovieComponent,
    TableComponent
  ],
  imports: [
    CommonModule,
    MoviesRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    SharedModule,
  ]
})
export class MoviesModule { }
