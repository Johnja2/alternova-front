import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs';
import { HandleErrorService } from 'src/app/common/services/handle-error.service';
import { ResponseDto } from 'src/app/common/types/reponse-dto.interface';
import { environment } from 'src/environments/environment';
import { IMovie, IMovieUpdate } from '../types/movie.interface';

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  baseUrl = environment.baseUrl;

  get token(): string {
    return localStorage.getItem('token') || '';
  }

  get uuid(): string {
    return localStorage.getItem('uuid') || '';
  }

  get headers() {
    return {
      headers: {
        'Authorization': this.token
      }
    }
  }

  constructor(private http: HttpClient, private handleErrorService: HandleErrorService) { }

  public getAllMovies() {
    return this.http.get<ResponseDto<IMovie[]>>(`${this.baseUrl}/movie`, this.headers).pipe(
      catchError(
        this.handleErrorService.handleError
      )
    );
  }

  public createMovie(movieData: IMovie) {
    return this.http.post<ResponseDto<IMovie>>(`${this.baseUrl}/movie`, movieData, this.headers).pipe(
      catchError(
        this.handleErrorService.handleError
      )
    );
  }


  public getMovieById(id: string) {
    return this.http.get<ResponseDto<IMovie>>(`${this.baseUrl}/movie/${id}`, this.headers).pipe(
      catchError(
        this.handleErrorService.handleError
      )
    );
  }

  public markAsViewOrScoreMovie(movieData: IMovieUpdate) {
    return this.http.patch<ResponseDto<IMovie>>(`${this.baseUrl}/movie`, movieData, this.headers).pipe(
      catchError(
        this.handleErrorService.handleError
      )
    );
  }
}

